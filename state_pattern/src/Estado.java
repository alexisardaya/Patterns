/**
 * Created by ALEXIS ARDAYA on 22/4/2018.
 */
public class Estado {
  private EstadoBase estado;

  public Estado(EstadoBase estado) {
    this.estado = estado;
  }

  public void cambiarEstado(EstadoBase ns){
    this.estado = ns;
  }

  public void habilitar(){
    estado.abrir();
    estado.enEstado();
    estado.cerrar();
  }
}

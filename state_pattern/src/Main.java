public class Main {

    public static void main(String[] args) {
        Estado e = new Estado(new EstadoA());
        e.habilitar();
        e.cambiarEstado(new EstadoB());
        e.habilitar();
    }
}

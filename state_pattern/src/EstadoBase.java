/**
 * Created by ALEXIS ARDAYA on 22/4/2018.
 */
public interface EstadoBase {
  void abrir();
  void enEstado();
  void cerrar();
}

/**
 * Created by ALEXIS ARDAYA on 22/4/2018.
 */
public class EstadoA implements EstadoBase{
  @Override
  public void abrir(){
    System.out.println("En transición a A");
  }

  @Override
  public void enEstado(){
    System.out.println("En estado A");
  }

  @Override
  public void cerrar(){
    System.out.println("A esta cerrado");
  }
}

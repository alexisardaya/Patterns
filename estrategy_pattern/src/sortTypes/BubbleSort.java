package sortTypes;

import java.util.List;

/**
 * Created by ALEXIS ARDAYA on 15/4/2018.
 */
public class BubbleSort implements SortingAlgorithm{

  @Override
  public List<Integer> sort(List<Integer> numbers) {
    return bubbleSort(numbers);
  }

  private List<Integer> bubbleSort(List<Integer> numbers){
    int countSwap=0;
    for (boolean isSort=false;!isSort;){
      for (int i=0;i<numbers.size()-1;i++){
        if (numbers.get(i)>numbers.get(i+1)){
          int auxValue=numbers.get(i);
          numbers.set(i, numbers.get(i+1));
          numbers.set(i+1, auxValue);
          countSwap++;
        }
      }
      if (countSwap==0){
        isSort=true;
      }
      countSwap=0;
    }
    return numbers;
  }
}

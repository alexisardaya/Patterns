package sortTypes;

import java.util.List;

/**
 * Created by ALEXIS ARDAYA on 15/4/2018.
 */
public class QuickSort implements SortingAlgorithm {

  @Override
  public List<Integer> sort(List<Integer> numbers) {
    quickSortNumbers(numbers, 0, numbers.size() - 1);
    return numbers;
  }

  private void quickSortNumbers(List<Integer> numbers, int left, int right) {
    int i = left;
    int j = right;
    int pivote = numbers.get(left);
    int swap;
    while (i < j) {
      while (numbers.get(i) <= pivote && i < j) {
        i++;
      }
      while (numbers.get(j) > pivote) {
        j--;
      }
      if (i < j) {
        swap = numbers.get(i);
        numbers.set(i, numbers.get(j));
        numbers.set(j, swap);
      }
    }
    numbers.set(left, numbers.get(j));
    numbers.set(j, pivote);
    if (left < j - 1) {
      quickSortNumbers(numbers, left, j - 1);
    }
    if (j + 1 < right) {
      quickSortNumbers(numbers, j + 1, right);
    }
  }
}

package sortTypes;

import java.util.List;

/**
 * Created by ALEXIS ARDAYA on 15/4/2018.
 */
public interface SortingAlgorithm {
  List<Integer> sort(List<Integer> numbers);
}

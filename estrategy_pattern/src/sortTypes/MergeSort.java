package sortTypes;

import java.util.List;

/**
 * Created by ALEXIS ARDAYA on 15/4/2018.
 */
public class MergeSort implements SortingAlgorithm {

  @Override
  public List<Integer> sort(List<Integer> numbers) {
    mergeSort(numbers);
    return numbers;
  }

  private List<Integer> mergeSort(List<Integer> numbers) {
    for (int i = 0; i < (numbers.size() - 1); i++) {
      for (int j = i + 1; j < numbers.size(); j++) {
        if (numbers.get(i) > numbers.get(j)) {
          int auxValue = numbers.get(i);
          numbers.set(i, numbers.get(j));
          numbers.set(j, auxValue);
        }
      }
    }
    return numbers;
  }
}

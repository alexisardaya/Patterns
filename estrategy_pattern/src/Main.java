import sortTypes.BubbleSort;
import sortTypes.MergeSort;
import sortTypes.QuickSort;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class Main extends JFrame {
  private static final int width = 600;
  private static final int height = 300;
  private JComboBox<String> comboType;
  private JLabel numbersLabel;
  private static final int LIST_SIZE = 7;
  private List<Integer> numberList;
  private ShortArray shortArray;

  public Main(String title) throws HeadlessException {
    super(title);
    this.shortArray = new ShortArray();
    this.comboType = createComboType();
    this.numberList = fillArray();
    this.numbersLabel = new JLabel(numberList.toString());
    this.initForm();
  }

  public static void main(String[] args) {
    JFrame frame = new Main("Estrategy Pattern");
    frame.setSize(width, height);
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    frame.setLocation(screenSize.width / 2 - width / 2,
            screenSize.height / 2 - height / 2);
    frame.show();
  }

  private void initForm() {
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(comboType, BorderLayout.NORTH);
    getContentPane().add(numbersLabel, BorderLayout.CENTER);
  }

  private JComboBox<String> createComboType() {
    JComboBox<String> type = new JComboBox<>();
    type.addItem("Select Type/ New List");
    type.addItem("QuickSort");
    type.addItem("BubbleSort");
    type.addItem("MergeSort");
    type.addActionListener(new SelectSortTypeListener());
    return type;
  }

  class SelectSortTypeListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
      JComboBox<String> comboType = (JComboBox<String>) e.getSource();
      String selectedType = (String) comboType.getSelectedItem();
      System.out.println(selectedType);
      if (selectedType.equals("Select Type/ New List")) {
        numberList = fillArray();
        numbersLabel.setText(numberList.toString());
      }
      if (selectedType.equals("QuickSort")) {
        shortArray.setSortingAlgorithm(new QuickSort());
        numbersLabel.setText(shortArray.sort(numberList).toString());
      }
      if (selectedType.equals("BubbleSort")) {
        shortArray.setSortingAlgorithm(new BubbleSort());
        numbersLabel.setText(shortArray.sort(numberList).toString());
      }
      if (selectedType.equals("MergeSort")) {
        shortArray.setSortingAlgorithm(new MergeSort());
        numbersLabel.setText(shortArray.sort(numberList).toString());
      }
    }

  }

  private List<Integer> fillArray() {
    List<Integer> number = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      number.add(randomNumber());
    }
    return number;
  }

  private int randomNumber() {
    return ((int) Math.floor(Math.random() * 500));
  }
}

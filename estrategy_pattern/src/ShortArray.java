import sortTypes.SortingAlgorithm;

import java.util.List;

/**
 * Created by ALEXIS ARDAYA on 15/4/2018.
 */
public class ShortArray {
  private SortingAlgorithm sortingAlgorithm;

  public ShortArray() {
  }

  public List<Integer> sort(List<Integer> numbers) {
    return this.sortingAlgorithm.sort(numbers);
  }

  public void setSortingAlgorithm(SortingAlgorithm sortingAlgorithm) {
    this.sortingAlgorithm = sortingAlgorithm;
  }
}
